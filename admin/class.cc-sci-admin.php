<?php
class CC_Sci_Admin
{
    private $plugin_name;
    private $version;

    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    public function enqueue_styles()
    {
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/cc-sci-admin.css', array(), $this->version, 'all');
    }

    public function enqueue_scripts()
    {
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/cc-sci-admin.js', array('jquery'), $this->version, true);
        wp_localize_script($this->plugin_name, $this->plugin_name, array(
            'vars' => 'vars',
            'nonce' => wp_create_nonce($this->plugin_name . '_ajax_nonce'),
        ));

        // Codemirror
        $settings = wp_enqueue_code_editor(array(
            'type' => 'htmlmixed',
            'codemirror' => array(
                'indentUnit' => 2,
                'tabSize' => 2,
            ),
        ));
        if ($settings) {
            wp_add_inline_script(
                'code-editor', sprintf(
                    'jQuery( function() { jQuery(\'[name="cc_sci_html"]\').each(function() { wp.codeEditor.initialize( jQuery(this), %s); }); });', wp_json_encode($settings)
                )
            );
        }
    }

    public function add_admin_bar_menu($admin_bar)
    {
        
    }

    public function add_plugin_admin_menu()
    {
//        add_options_page('Script Injector Setup', 'Script Injector', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page')
//        );
    }

    public function add_action_links($links)
    {
        // Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
//        $settings_link = array(
//            '<a href="' . admin_url('options-general.php?page=' . $this->plugin_name) . '">' . __('Settings', $this->plugin_name) . '</a>',
//        );
//        return array_merge($settings_link, $links);
    }

    public function display_plugin_setup_page()
    {
        include_once( 'partials/admin-display.php' );
    }

    public function columns($columns)
    {
        return array_merge(array_slice($columns, 0, -1), array(
            'position' => __('Position', $this->plugin_name),
            'content_length' => __('Code length', $this->plugin_name),
            ), array_slice($columns, -1));
    }

    public function custom_column($column, $post_id)
    {
        switch ($column) {
            case 'position':
                echo get_post_meta($post_id, '_' . $this->plugin_name . '_position', true);
                break;

            case 'content_length':
                $char_count = strlen(get_post_meta($post_id, '_' . $this->plugin_name . '_html', true));
                echo sprintf(esc_html__(_n('%d character', '%d characters', $char_count, $this->plugin_name)), $char_count);
                break;
        }
    }

    public function add_meta_boxes()
    {
        add_meta_box('position_meta_box', __('Position', $this->plugin_name), array($this, 'build_position_meta_box'), $this->plugin_name);
        add_meta_box('location_meta_box', __('Enable on', $this->plugin_name), array($this, 'build_location_meta_box'), $this->plugin_name);
        add_meta_box('html_meta_box', __('HTML code', $this->plugin_name), array($this, 'build_html_meta_box'), $this->plugin_name);
    }

    public function build_html_meta_box($post)
    {
        $value = get_post_meta($post->ID, '_' . $this->plugin_name . '_html', true);
        ?>
        <label class="screen-reader-text" for="excerpt"><?php echo __('HTML', $this->plugin_name) ?></label>
        <textarea rows="12" cols="40" name="<?php echo $this->plugin_name ?>_html" style="width: 100%;"><?php echo $value ?></textarea>
        <p>
            <?php _e('Make sure your code is valid HTML.<br />Insert your scrips between <code>&lt;script&gt;</code> tags.', $this->plugin_name); ?>
        </p>

        <?php
    }

    public function build_position_meta_box($post)
    {
        $position = get_post_meta($post->ID, '_' . $this->plugin_name . '_position', true);
        ?>

        <label class="screen-reader-text"><?php echo __('Insert to', $this->plugin_name) ?></label>
        <select name="<?php echo $this->plugin_name ?>_position">
            <optgroup label="<?php _e('Standard positions', $this->plugin_name) ?>">
                <option value="before_wp_head" <?php selected($position, 'before_wp_head') ?>><?php _e('Beginning of head', $this->plugin_name) ?></option>
                <option value="after_wp_head" <?php selected($position, 'after_wp_head') ?>><?php _e('End of head', $this->plugin_name) ?></option>
                <option value="before_wp_footer" <?php selected($position, 'before_wp_footer') ?>><?php _e('Beginning of footer', $this->plugin_name) ?></option>
                <option value="after_wp_footer" <?php selected($position, 'after_wp_footer') ?>><?php _e('End of footer', $this->plugin_name) ?></option>
            </optgroup>
            <optgroup label="<?php _e('For experts only', $this->plugin_name) ?>">
                <option value="after_body_begin_with_hook" <?php selected($position, 'after_body_begin_with_hook') ?>><?php _e('After the opening body tag with hook', $this->plugin_name) ?></option>
                <option value="after_body_begin_with_body_class" <?php selected($position, 'after_body_begin_with_body_class') ?>><?php _e('After the opening body tag with body class', $this->plugin_name) ?></option>
            </optgroup>
        </select>

        <div class="<?php echo $this->plugin_name ?>_position_warning notice-warning">
            <?php _e("<p>We try to inject your script right after the body tag, but Wordpress currently does not have a native method for this. <br />Luckily, you have two options:</p>
<h4>1. If you have access to your template files - with hook</h4>
<p>Add the following code after your body tag (probably in your header.php file): <code>&lt;?php cc_sci_after_body_open() ?&gt;</code></p>
<h4>2. If you can't modify your template files - with body class</h4>
<p>There is an alternate solution that takes a fair assumption:<br />Your theme's opening <code>&lt;body&gt;</code> tag must look like this: <code>&lt;body ... &lt;?php body_class(); ?&gt;&gt;</code><br />For most scenarios this should be the case as WordPress asks to implement the <code>body_class()</code> template tag when developing a theme.</p>
<p>However, please make sure that your script is located and works properly after applying any of the solutions above.</p>", $this->plugin_name); ?>
        </div>
        <?php
    }

    public function build_location_meta_box($post)
    {
        $location = get_post_meta($post->ID, '_' . $this->plugin_name . '_location', true);
        $data = json_decode($location, true);
        if (!$data) {
            $data = array();
        }
        ?>

        <?php wp_nonce_field($this->plugin_name . '_meta_nonce', $this->plugin_name . '_meta_nonce'); ?>

        <input type="text" name="<?php echo $this->plugin_name ?>_location" class="<?php echo $this->plugin_name ?>_location" />
        <?php foreach ($data as $group): ?>
            <?php echo $this->group($group); ?>
        <?php endforeach; ?>
        <p><a href="#" class="button <?php echo $this->plugin_name ?>_add_location_group"><?php _e('Add rule group', $this->plugin_name) ?></a></p>
        <?php
    }

    public function register_custom_post_type()
    {
        $labels = array(
            'name' => __('Scripts', $this->plugin_name),
            'singular_name' => __('Script', $this->plugin_name),
            'menu_name' => __('Scripts', $this->plugin_name),
            'parent_item_colon' => __('Parent Script:', $this->plugin_name),
            'all_items' => __('All Script', $this->plugin_name),
            'view_item' => __('View Script', $this->plugin_name),
            'add_new_item' => __('Add New Script', $this->plugin_name),
            'add_new' => __('Add New', $this->plugin_name),
            'edit_item' => __('Edit Script', $this->plugin_name),
            'update_item' => __('Update Script', $this->plugin_name),
            'search_items' => __('Search Script', $this->plugin_name),
            'not_found' => __('Not found', $this->plugin_name),
            'not_found_in_trash' => __('Not found in Trash', $this->plugin_name),
        );
        $args = array(
            'label' => __('Script', $this->plugin_name),
            'description' => __('Post Type for HTML injected to wp_head or wp_footer', $this->plugin_name),
            'labels' => $labels,
            'supports' => array('title',),
            'taxonomies' => array(),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true, //'tools.php',
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => false,
            'menu_position' => 100,
            'menu_icon' => 'dashicons-email',
            'can_export' => true,
            'has_archive' => false,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'capability_type' => 'post',
        );
        register_post_type($this->plugin_name, $args);
    }

    public function ajax_location_values()
    {
        if (!check_ajax_referer($this->plugin_name . '_ajax_nonce', 'nonce')) {
            exit;
        }
        echo $this->value($_POST['type']);
        exit;
    }

    public function ajax_location_rule()
    {
        if (!check_ajax_referer($this->plugin_name . '_ajax_nonce', 'nonce')) {
            exit;
        }
        echo $this->rule();
        exit;
    }

    public function ajax_location_group()
    {
        if (!check_ajax_referer($this->plugin_name . '_ajax_nonce', 'nonce')) {
            exit;
        }
        echo $this->group();
        exit;
    }

    public function group($group = array())
    {
        $html = '<div class="' . $this->plugin_name . '_location_group"><table><tbody>';
        if (empty($group)) {
            $html .= $this->rule();
        } else {
            foreach ($group as $rule) {
                $html .= $this->rule($rule['type'], $rule['operator'], $rule['value']);
            }
        }
        $html .= '</tbody></table>';
        $html .= '<p><b>vagy</b></p>';
        $html .= '</div>';
        return $html;
    }

    public function rule($type = 'post_type', $operator = '==', $value = null)
    {
        $html = '<tr class="' . $this->plugin_name . '_location_rule">';
        $html .= '<td>' . $this->type($type) . '</td>';
        $html .= '<td>' . $this->operator($operator) . '</td>';
        $html .= '<td><div class="' . $this->plugin_name . '_location_value_wrapper">' . $this->value($type, $value) . '</div></td>';
        $html .= '<td><a href="#" class="button ' . $this->plugin_name . '_add_location_rule">és</a></td>';
        $html .= '<td><a href="#" class="' . $this->plugin_name . '_remove_location_rule"><span class="dashicons dashicons-no"></span></a></td>';
        $html .= '</tr>';
        return $html;
    }

    public function type($selected = null)
    {
        $types = array(
            'post_type' => __('Post type', $this->plugin_name),
            'post_template' => __('Template', $this->plugin_name),
            'is' => __('Current page', $this->plugin_name),
        );

        $html = '<select class="' . $this->plugin_name . '_location_type" name="' . $this->plugin_name . '_location_type">';
        foreach ($types as $value => $label) {
            $html .= '<option value="' . $value . '"' . selected($value, $selected, false) . '>' . $label . '</option>';
        }
        $html .= '</select>';

        return $html;
    }

    public function operator($selected = null)
    {
        $operators = array(
            '==' => __('equals', $this->plugin_name),
            '!=' => __('not equals', $this->plugin_name),
        );

        $html = '<select class="' . $this->plugin_name . '_location_operator" name="' . $this->plugin_name . '_location_operator">';
        foreach ($operators as $value => $label) {
            $html .= '<option value="' . $value . '"' . selected($value, $selected, false) . '>' . $label . '</option>';
        }
        $html .= '</select>';

        return $html;
    }

    public function value($type, $selected = null)
    {
        if ($type === 'post_type') {
            $post_types = get_post_types(array(
                'public' => true,
                ), 'object');

            foreach ($post_types as $post_type) {
                $values[$post_type->name] = $post_type->label;
            }
        }

        if ($type === 'post_template') {
            $templates = get_page_templates();

            foreach ($templates as $label => $template) {
                $values[$template] = $label;
            }
        }

        if ($type === 'is') {
            $values = array(
                'is_home' => __('Home', $this->plugin_name),
                'is_front_page' => __('Front page', $this->plugin_name),
                'is_404' => __('404', $this->plugin_name),
                'is_search' => __('Search Results', $this->plugin_name),
                'is_date' => __('Date archive', $this->plugin_name),
                'is_author' => __('Author', $this->plugin_name),
                'is_category' => __('Category', $this->plugin_name),
                'is_tag' => __('Tag', $this->plugin_name),
                'is_archive' => __('Archive', $this->plugin_name),
                'is_singular' => __('Singular', $this->plugin_name),
                'is_attachment' => __('Attachment', $this->plugin_name),
            );
        }

        $html = '<select class="' . $this->plugin_name . '_location_value" name="' . $this->plugin_name . '_location_value">';
        foreach ($values as $value => $label) {
            $html .= '<option value="' . $value . '"' . selected($value, $selected, false) . '>' . $label . '</option>';
        }
        $html .= '</select>';

        return $html;
    }

    public function save_post_meta($post_id, $post)
    {
        $nonce = $this->plugin_name . '_meta_nonce';

        if (!isset($_POST[$nonce])) {
            return $post_id;
        }

        if (!wp_verify_nonce($_POST[$nonce], $nonce)) {
            return $post_id;
        }

        $post_type = get_post_type_object($post->post_type);

        if (!current_user_can($post_type->cap->edit_post, $post_id)) {
            return $post_id;
        }

        $this->save_post_meta_values($post_id);
    }

    public function save_post_meta_values($post_id)
    {
        $html_key = $this->plugin_name . '_html';
        if (isset($_POST[$html_key])) {
            update_post_meta($post_id, '_' . $html_key, $_POST[$html_key]);
        }

        $position_key = $this->plugin_name . '_position';
        if (isset($_POST[$position_key])) {
            update_post_meta($post_id, '_' . $position_key, $_POST[$position_key]);
        }

        $location_key = $this->plugin_name . '_location';
        if (isset($_POST[$location_key])) {
            update_post_meta($post_id, '_' . $location_key, $_POST[$location_key]);
        }
    }
}
