(function ($) {

    /**
     * Update location group items
     */
    var update = function () {
        var groups = [];
        $('.cc_sci_location_group').each(function () {
            var $group = $(this);
            var rules = [];
            $group.find('.cc_sci_location_rule').each(function () {
                var $rule = $(this);
                rules.push({
                    type: $rule.find('[name="cc_sci_location_type"]').val(),
                    operator: $rule.find('[name="cc_sci_location_operator"]').val(),
                    value: $rule.find('[name="cc_sci_location_value"]').val()
                });
            });
            groups.push(rules);
        });

        $('[name="cc_sci_location"]').val(JSON.stringify(groups));
    };

    update();

    $(document).on('change', '.cc_sci_location_operator, .cc_sci_location_value', function () {
        update();
    });

    $(document).on('change', '.cc_sci_location_type', function () {
        var $select = $(this);
        var type = $select.val();
        var $wrapper = $select.closest('tr').find('.cc_sci_location_value_wrapper');
        $wrapper.empty().append('<span class="cc_sci_spinner"></span>');

        $.ajax({
            type: 'POST',
            url: window.ajaxurl,
            data: {
                action: 'cc_sci_location_values',
                type: type,
                nonce: window.cc_sci.nonce
            },
            success: function (response) {
                $wrapper.empty().append(response);
                update();
            }
        });
    });

    $(document).on('click', '.cc_sci_add_location_rule', function (e) {
        e.preventDefault();

        var $caller = $(e.target).closest('tr');
        $caller.after('<span class="cc_sci_spinner"></span>');

        $.ajax({
            type: 'POST',
            url: window.ajaxurl,
            data: {
                action: 'cc_sci_location_rule',
                nonce: window.cc_sci.nonce
            },
            success: function (response) {
                $caller.after(response);
                $caller.siblings('.cc_sci_spinner').remove();
                update();
            }
        });
    });

    $(document).on('click', '.cc_sci_add_location_group', function (e) {
        e.preventDefault();

        var $caller = $(e.target).closest('p');
        $caller.before('<span class="cc_sci_spinner"></span>');

        $.ajax({
            type: 'POST',
            url: window.ajaxurl,
            data: {
                action: 'cc_sci_location_group',
                nonce: window.cc_sci.nonce
            },
            success: function (response) {
                $caller.before(response);
                $caller.siblings('.cc_sci_spinner').remove();
                update();
            }
        });
    });

    $(document).on('click', '.cc_sci_remove_location_rule', function (e) {
        e.preventDefault();

        var $rule = $(e.target).closest('.cc_sci_location_rule');
        var $group = $rule.closest('.cc_sci_location_group');

        $rule.remove();
        if ($group.find('.cc_sci_location_rule').length === 0) {
            $group.remove();
        }

        update();
    });

    /*
     * Warning message
     */
    var updateWarning = function () {
        debugger;
        if ($('[name="cc_sci_position"]').find('option:selected').filter('[value^="after_body_begin"]').length > 0) {
            $('.cc_sci_position_warning').show();
        } else {
            $('.cc_sci_position_warning').hide();
        }
    };

    updateWarning();

    $(document).on('change', '[name="cc_sci_position"]', function (e) {
        updateWarning();
    });

})(jQuery);
