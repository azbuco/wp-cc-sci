<?php

class CC_Sci_Public
{
    private $plugin_name;
    private $version;

    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;

        require_once dirname(__FILE__) . '/partials/cc-sci-public-functions.php';
    }

    public function enqueue_styles()
    {
        wp_enqueue_style($this->plugin_name . '_public', plugin_dir_url(__FILE__) . 'css/cc-sci-public.css', array(), $this->version, 'all');
    }

    public function enqueue_scripts()
    {
        wp_enqueue_script($this->plugin_name . '_public', plugin_dir_url(__FILE__) . 'js/cc-sci-public.js', array('jquery'), $this->version, false);
    }

    public function parser()
    {
        $scripts = get_posts(array(
            'post_type' => $this->plugin_name,
        ));

        $total = count($scripts);
        $active_scripts = [];

        foreach ($scripts as $script) {
            $location = get_post_meta($script->ID, '_' . $this->plugin_name . '_location', true);
            $groups = json_decode($location);
            if ($this->parse($groups)) {
                $html = get_post_meta($script->ID, '_' . $this->plugin_name . '_html', true);
                $position = get_post_meta($script->ID, '_' . $this->plugin_name . '_position', true);
                $this->add($html, $position);
                $active_scripts[] = $script;
            }
        }

        add_action('admin_bar_menu', function($admin_bar) use ($total, $active_scripts) {
            $active_count = count($active_scripts);
            $title = __('Scripts', $this->plugin_name) . ' (' . $total . '/' . $active_count . ')';

            $admin_bar->add_node(array(
                'id' => $this->plugin_name . 'admin_bar_menu',
                'title' => $title,
                'href' => admin_url('edit.php?post_type=cc_sci'),
            ));
            
            foreach($active_scripts as $script) {
                $admin_bar->add_node(array(
                    'parent' => $this->plugin_name . 'admin_bar_menu',
                    'id' => $this->plugin_name . 'admin_bar_menu_' . $script->ID,
                    'title' => $script->post_title,
                    'href' => get_edit_post_link($script->ID),
                ));
            }
        }, 999);
    }

    public function add($html, $position)
    {
        switch ($position) {
            case 'before_wp_head':
                add_action('wp_head', function() use ($html) {
                    echo $html;
                }, 10);
                break;

            case 'after_wp_head':
                add_action('wp_head', function() use ($html) {
                    echo $html;
                }, 90);
                break;

            case 'before_wp_footer':
                add_action('wp_footer', function() use ($html) {
                    echo $html;
                }, 10);
                break;

            case 'after_wp_footer':
                add_action('wp_footer', function() use ($html) {
                    echo $html;
                }, 90);
                break;

            case 'after_body_begin_with_hook':
                add_action('cc_sci_after_body_open', function() use ($html) {
                    echo $html;
                });
                break;

            case 'after_body_begin_with_body_class':
                add_filter('body_class', function($classes) use ($html) {
                    $classes[] = '">' . $html . '<br style="display:none';
                    return $classes;
                }, 10000);
                break;
        }
    }

    public function parse($groups)
    {
        if (empty($groups)) {
            return true;
        }
        $or = false;
        foreach ($groups as $gi => $group) {
            $and = true;
            foreach ($group as $ri => $rule) {
                $and = $and && $this->evaluate($rule->type, $rule->operator, $rule->value);
            }
            $or = $or || $and;
        }
        return $or;
    }

    public function evaluate($type, $operator, $value)
    {
        global $post;

        if ($type === 'post_type') {
            if (is_archive()) {
                return $this->compare($operator, is_post_type_archive($value), true);
            } else {
                return $this->compare($operator, is_singular($value), true);
            }
        }

        if ($type === 'post_template') {
            return $this->compare($operator, is_page_template($value), true);
        }

        if ($type === 'is') {
            $function = $value;
            return $this->compare($operator, $function(), true);
        }
    }

    public function compare($operator, $value1, $value2)
    {
        if ($operator == '==') {
            return $value1 == $value2;
        }
        if ($operator == '!=') {
            return $value1 != $value2;
        }
        return false;
    }
}
