��    1      �  C   ,      8     9     T  �  X     �     �     �       *     $   H  
   m     x  
   �     �     �     �     �     �     �     �     �  	   �     �     	     	  
   (	     3	  	   8	     B	  	   G	  d   Q	  	   �	     �	     �	     �	  3   �	  	   
     )
     0
     8
     G
     U
     ^
     q
     u
     ~
     �
     �
  
   �
  v  �
     !     .    <     U     e     v     �  2   �      �     �          %     7  #   U     y     �     �     �     �     �     �  %     !   3     U     q     �  	   �     �     �     �     6     F     c  	   r  2   |     �     �     �     �     �     �          &     9     @     U     j     s         '              .                       #                                   +       	       
   )   (             *   $                         /            %   !         1          "                            0      -   ,   &       %d character %d characters 404 <p>We try to inject your script right after the body tag, but Wordpress currently does not have a native method for this. <br />Luckily, you have two options:</p>
<h4>1. If you have access to your template files - with hook</h4>
<p>Add the following code after your body tag (probably in your header.php file): <code>&lt;?php cc_sci_after_body_open() ?&gt;</code></p>
<h4>2. If you can't modify your template files - with body class</h4>
<p>There is an alternate solution that takes a fair assumption:<br />Your theme's opening <code>&lt;body&gt;</code> tag must look like this: <code>&lt;body ... &lt;?php body_class(); ?&gt;&gt;</code><br />For most scenarios this should be the case as WordPress asks to implement the <code>body_class()</code> template tag when developing a theme.</p>
<p>However, please make sure that your script is located and works properly after applying any of the solutions above.</p> Active scripts Add New Add New Script Add rule group After the opening body tag with body class After the opening body tag with hook All Script Archive Attachment Author Beginning of footer Beginning of head Category Code length Current page Date archive Edit Script Enable on End of footer End of head For experts only Front page HTML HTML code Home Insert to Make sure your code is valid HTML.<br />Insert your scrips between <code>&lt;script&gt;</code> tags. Not found Not found in Trash Parent Script: Position Post Type for HTML injected to wp_head or wp_footer Post type Script Scripts Search Results Search Script Singular Standard positions Tag Template Update Script View Script equals not equals Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-01-02 15:27+0100
PO-Revision-Date: 2018-01-02 15:31+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: plurals=2; plural=(n > 1)
X-Generator: Eazy Po 0.9.5.3
 %d karakter  404 hibaoldal <p>Jelenleg a WordPressben nincs olyan megoldás, ami közvetlen a body tag után írna.<br /> Szerencsére két alternatív megoldás is van erre, bár mindkettő csak korlátozottan alkalmazható:</p>
<h4>1. Ha van hozzáférésed a sablon fájlokhoz - akkor egyedi hurokkal</h4>
<p>Add a következő kódot a közvetlen a nyitó body tag után (valószínüleg a header.php fájlban): <code>&lt;?php cc_sci_after_body_open() ?&gt;</code></p>
<h4>2. Ha nem tudod módosítani a template fájljaidat - akkor body css osztályon keresztül </h4>
<p>Bár az első a jobb megoldás, de ha valamért nem menne megpróbálkozhatsz ezzel is. Ez a megoldás a következő feltételezésre épül: a témád nyitó <code> &lt;body&gt;</code> tagjának így kell kinéznie: <code>&lt;body ... &lt;?php body_class(); ? &gt;&gt;</code><br />A legtöbb esetben erre van némi esély hiszen a WordPress ajánlásaiban ez szerepel.</p>
<p>Mindenesetre ha ezeket az opciókat választod, feltétlen ellenőrizd le, hogy minden továbbra is rendben működik!</p> Aktív scriptek Új hozzáadása Új script hozzáadása Szabálycsoport hozzáadása A nyitó body tag után body class segítségével A nyitó body tag után hurokkal Minden script Bejegyzések archívuma Csatolmány oldal Szerző szerinti bejegyzések Oldal aljára elsőként (body tag) Fejlécbe elsőként (head tag) Kategória bejegyzései Kód hossza Aktuális oldal Dátum szerinti bejegyzések Script szerkesztése Engedélyezve Oldal aljára utolsóként (body tag) Fejlécbe utolsóként (head tag) Csak ha tudod mit csinálsz Főoldal (front page) HTML HTML kód Kezdőlap (home) Beszúrás ide Érvényes HTML kódot használj.<br />A scripteket ne közvetlenül, hanem <code>&lt;script&gt; </code> tagek közt használd. Nem található Nem található a lomtárban Szülő script Pozíció Bejegyzés típus HTML-be illeszthető scriptekhez Bejegyzés típus Scirpt Scriptek Keresési találatok Script keresése Egyedi oldal (singular) Általános pozíciók Cimke bejegyzései Sablon Script módosítása Script megtekintése egyenlő nem egyenlő 