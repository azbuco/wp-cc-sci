<?php

class CC_Sci
{

    protected $loader;
    protected $plugin_name;
    protected $version;

    public function __construct()
    {
        if (defined('CC_SCI_VERSION')) {
            $this->version = CC_SCI_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->plugin_name = 'cc_sci';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    private function load_dependencies()
    {
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class.cc-sci-loader.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class.cc-sci-i18n.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class.cc-sci-admin.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class.cc-sci-public.php';

        $this->loader = new CC_Sci_Loader();
    }

    private function set_locale()
    {
        $plugin_i18n = new CC_Sci_I18n($this->plugin_name);
        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }

    private function define_admin_hooks()
    {
        $plugin_admin = new CC_Sci_Admin($this->plugin_name, $this->version);

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

        // Add custom post type
        $this->loader->add_action('init', $plugin_admin, 'register_custom_post_type');

        // Custom metaboxes
        $this->loader->add_action('add_meta_boxes_' . $this->plugin_name, $plugin_admin, 'add_meta_boxes');
        $this->loader->add_action('save_post', $plugin_admin, 'save_post_meta', 10, 2);

        $this->loader->add_filter('manage_' . $this->plugin_name . '_posts_columns', $plugin_admin, 'columns');
        $this->loader->add_action('manage_' . $this->plugin_name . '_posts_custom_column', $plugin_admin, 'custom_column', 10, 2);
        
        // Add ajax handlers
        $this->loader->add_action('wp_ajax_' . $this->plugin_name . '_location_values', $plugin_admin, 'ajax_location_values');
        $this->loader->add_action('wp_ajax_' . $this->plugin_name . '_location_rule', $plugin_admin, 'ajax_location_rule');
        $this->loader->add_action('wp_ajax_' . $this->plugin_name . '_location_group', $plugin_admin, 'ajax_location_group');

        // Add admin bar item
        $this->loader->add_action('admin_bar_menu', $plugin_admin, 'add_admin_bar_menu', 999);
        
        // Add menu item
//        $this->loader->add_action('admin_menu', $plugin_admin, 'add_plugin_admin_menu');
       
        // Add Settings link to the plugin
//        $plugin_basename = plugin_basename(plugin_dir_path(__DIR__) . $this->plugin_name . '.php');
//        $this->loader->add_filter('plugin_action_links_' . $plugin_basename, $plugin_admin, 'add_action_links');
    }

    private function define_public_hooks()
    {
        $plugin_public = new CC_Sci_Public($this->plugin_name, $this->version);

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
        
        $this->loader->add_action('get_header', $plugin_public, 'parser');
    }

    public function run()
    {
        $this->loader->run();
    }

}
