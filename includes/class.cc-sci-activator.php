<?php

/**
 * Fired during plugin activation
 *
 * @link       d
 * @since      1.0.0
 *
 * @package    Cc_script_injector
 * @subpackage Cc_script_injector/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cc_script_injector
 * @subpackage Cc_script_injector/includes
 * @author     CC <azbuco@gmail.com>
 */
class CC_Sci_Activator
{

    public static function activate()
    {
        
    }

}
