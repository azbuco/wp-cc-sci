<?php

/**
 * Define the internationalization functionality.
 */
class CC_Sci_I18n
{
    protected $textdomain;

    public function __construct($textdomain)
    {
        $this->plugin_name = $textdomain;
    }

    public function load_plugin_textdomain()
    {
        $r = load_plugin_textdomain(
            $this->plugin_name, false, dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
        );
    }
}
