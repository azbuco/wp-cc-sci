<?php

/**
 * Plugin Name: CC Script Injector
 * Description: Inject JS, CSS or basicly any valid HTML code to Wordpress header and footer.
 * Version: 1.0.3
 * Author:  azbuco
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: cc_sci
 * Domain Path: /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

define('CC_SCI_VERSION', '1.0.2');

function activate_cc_sci()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class.cc-sci-activator.php';
    CC_Sci_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cc_sci-deactivator.php
 */
function deactivate_cc_sci()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class.cc-sci-deactivator.php';
    CC_Sci_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_cc_sci');
register_deactivation_hook(__FILE__, 'deactivate_cc_sci');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class.cc-sci.php';

$cc_sci = new CC_Sci();
$cc_sci->run();

/**
 * Plugin update
 */
require dirname(__FILE__) . '/plugin-update-checker/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/azbuco/wp-cc-sci',
	__FILE__,
	'wp-cc-sci',
        1
);

//Optional: If you're using a private repository, create an OAuth consumer
//and set the authentication credentials like this:
//Note: For now you need to check "This is a private consumer" when
//creating the consumer to work around #134:
// https://github.com/YahnisElsts/plugin-update-checker/issues/134
//$myUpdateChecker->setAuthentication(array(
//	'consumer_key' => '...',
//	'consumer_secret' => '...',
//));

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');