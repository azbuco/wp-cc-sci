=== wp-cc-sci ===
Contributors: azbuco
Tags: js, css
Requires at least: 4.3
Requires PHP: 5.4
License: MIT
License URI: https://opensource.org/licenses/MIT

Insert CSS and JS scripts to WordPress pages

== Description ==
Insert any valid HTML code to a WordPress site, and manage these in one place.

== Changelog ==
1.0.3
* On public pages the admin bar has a script node.

1.0.2
* Auto update from BitBucket.

1.0.1
* Localized to hu_HU.

1.0.0
* Initial release.